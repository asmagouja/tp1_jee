package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.beans.Client;

/**
 * Servlet implementation class CreationClient
 */
@WebServlet("/CreationClient")
public class CreationClient extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreationClient() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/creerClient.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.print("YESSSSSS");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("telephone");
		String email = request.getParameter("email");

		Client client = new Client(nom, prenom, telephone, email);
//NOTE: c'est la m�me chose que dessus!!
//      Client client = new Client();
//		client.setNom(nom);
//		client.setPrenom(prenom);
//		client.setTelephone(telephone);
//		client.setEmail(email);

		request.setAttribute("client", client);

		String message;
		if (nom.trim().isEmpty() || prenom.trim().isEmpty() || telephone.trim().isEmpty() || email.trim().isEmpty()) {
			message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creerClient.jsp\">Cliquez ici</a> pour acc�der au formulaire de cr�ation d'un client.";
		} else {
			message = "Client cr�� avec succ�s !";
		}
		request.setAttribute("message", message);

		this.getServletContext().getRequestDispatcher("/WEB-INF/afficherClient.jsp").forward(request, response);
//		doGet(request, response);//�a �a me fait retourner � doGet au dessus donc � creer client
	}

}
