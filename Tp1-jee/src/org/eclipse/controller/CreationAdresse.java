package org.eclipse.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.beans.Adresse;
import org.eclipse.beans.Client;

/**
 * Servlet implementation class CreationAdresse
 */
@WebServlet("/CreationAdresse")
public class CreationAdresse extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreationAdresse() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/creerAdresse.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String rue = request.getParameter("rue");
		String ville = request.getParameter("ville");
		String codePostal = request.getParameter("codePostal");

		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("telephone");
		String email = request.getParameter("email");
		String message;
		
		if (nom.trim().isEmpty() || prenom.trim().isEmpty() || telephone.trim().isEmpty() || email.trim().isEmpty()
				|| rue.isEmpty() || codePostal.isEmpty() || ville.isEmpty()) {
			message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires. <br> <a href=\"creerAdresse.jsp\">Cliquez ici</a> pour acc�der au formulaire de cr�ation d'une adresse.";
		} else {
			message = "Client cr�� avec succ�s !";
		}

//		Client client = new Client(nom, prenom, telephone, email);
//		Adresse adresse = new Adresse(rue, ville, codePostal, client);
		
		Client client = new Client();
		client.setNom(nom);
		client.setPrenom(prenom);
		client.setTelephone(telephone);
		client.setEmail(email);

		Adresse adresse = new Adresse();
		adresse.setClient(client);
		adresse.setRue(rue);
		adresse.setCodePostal(codePostal);
		adresse.setVille(ville);

		request.setAttribute("adresse", adresse);
		request.setAttribute("message", message);

		this.getServletContext().getRequestDispatcher("/afficherAdresse.jsp").forward(request, response);

	}

}
